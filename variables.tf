variable "environment" {
  type        = string
  description = "Environment name"
}

variable "name" {
  type        = string
  description = "Service name"
}

variable "cluster_id" {
  type        = string
  description = "Cluster ID to run the service"
}

variable "task_definition_arn" {
  type        = string
  description = "Task definition ARN"
}

variable "replication_factor" {
  type        = number
  description = "Number of instances to run"
  default     = 1
}

variable "launch_type" {
  type        = string
  description = "Launch type configuration [EC2, FARGATE]"
  default     = "EC2"
}

variable "subnets" {
  type        = list(string)
  description = "VPC subnets"
  default     = null
}

variable "security_groups" {
  type        = list(string)
  description = "VPC security groups"
  default     = null
}

variable "assign_public_ip" {
  type        = bool
  description = "Assign or not a security group"
  default     = false
}

variable "target_group_arn" {
  type        = string
  description = "Use or not a load balancer"
  default     = null
}

variable "port" {
  type        = number
  description = "Port to run on load balancer"
  default     = null
}
