terraform {
  required_version = ">= 0.13.2"
}

resource "aws_ecs_service" "service" {
  name                  = var.name
  cluster               = var.cluster_id
  task_definition       = var.task_definition_arn
  desired_count         = var.replication_factor
  launch_type           = var.launch_type

  dynamic "network_configuration" {
    for_each = var.subnets != null ? [1] : []
    content {
      subnets             = var.subnets
      security_groups     = var.security_groups
      assign_public_ip    = var.assign_public_ip
    }
  }

  dynamic "load_balancer" {
    for_each = var.target_group_arn != null ? [1] : []
    content {
      target_group_arn = var.target_group_arn
      container_name   = var.name
      container_port   = var.port
    }
  }

  tags = {
    Name              = var.name
    Environment       = var.environment
  }
}
