# ECS Service Module

This module is capable to create a new ECS service.

Module Input Variables
----------------------

- `environment` - environment name
- `name` - general service name
- `cluster_id` - cluster identifier
- `task_definition_arn` - task definition identifier
- `replication_factor` - number of instances to instantiate (default 1)
- `launch_type` - type of launch (default EC2)
- `network_configuration` - network type (default awsvpc)
- `target_group_arn` - target group arn (only for load balancer usage)
- `port` - port (only for load balancer usage)

Usage
-----

```hcl
module "service" {
  source              = "git::https://gitlab.com/mesabg-tfmodules/service.git"

  environment         = "environment"
  name                = "servicename"

  subnets             = ["subent-aaaa", "subnet-bbbb"]
  security_groups     = ["sg-xxxx", "sg-rrrr"]
  assign_public_ip    = false

  cluster_id          = "cluster-ddd"
  task_definition_arn = "arn::task"
}
```

```hcl
module "service" {
  source                = "git::https://gitlab.com/mesabg-tfmodules/service.git"

  environment           = "environment"
  name                  = "servicename"

  subnets               = ["subent-aaaa", "subnet-bbbb"]
  security_groups       = ["sg-xxxx", "sg-rrrr"]
  assign_public_ip      = false

  cluster_id            = "cluster-ddd"
  task_definition_arn   = "arn::task"
  replication_factor    = 3

  launch_type           = "FARGATE"
  target_group_arn      = "arn::sample"
  port                  = 24250
}
```

Outputs
=======

 - `service` - Created ECS service


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
